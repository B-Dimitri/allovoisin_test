import Vue from 'vue'
import App from './App.vue'
import './style/scss/init.scss';
import VueTouch from 'vue-touch';

Vue.config.productionTip = false
Vue.use(VueTouch);

new Vue({
  render: function (h) { return h(App) },
}).$mount('#app')
