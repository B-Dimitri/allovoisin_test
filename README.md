# Test pour Allovoisins

## A test, an exercise
Hi, i'm Dimitri Bahuaud, I'll do a test for Allovoisins.

## The techno choices
For the test, VueJS is needed, I'll use it with Sass, as usual.

## Difficulties
### Comprehension
With theses two screen, it's hard to see the interaction/animations between the composants.
I also removed the "Ajouter un article" button, I think there's a full store process that I don't have.
So, I've only set the "Enregistrer l'article" button.

### No css grid
For that project, I don't really saw where to use CSS Grids. The bars take the full width of theirs each components so..

## Time check
I've spend around 4 hours to do theses components
- 30mins to understand and think about the system i'll use before begin
- 5mins to create the project and send on github at the end
- 15mins to create/populate the README, after that i'll add some comments
- 1h for the bar/edit system
- 1h30 for the summ + datas
- 30mins to check the errors or details that I've missed

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

## Thanks :)
